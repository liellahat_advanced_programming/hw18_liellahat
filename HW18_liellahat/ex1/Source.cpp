#include "Header.h"


LPSTR cwd = new TCHAR[BUFFER_SIZE];


int main()
{
	vector<string> words;
	string command;

	while (true)
	{
		GetCurrentDirectory(BUFFER_SIZE, cwd);
		cout << cwd << "#";

		getline(cin, command);
		words = Helper::get_words(command);

		if (words.size() > 0)
		{
			executeCommand(words);
		}
	}

	system("pause");
	return 0;
}


void executeCommand(vector<string> words)
{
	int i = 0;
	int out = 0;
	HANDLE handle;
	HMODULE module;
	WIN32_FIND_DATA FindFileData;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	string cmdLine;
	LPDWORD exitCode = (LPDWORD)new int;
	func* f = NULL;

	if (words[0] == "quit")
	{
		exit(0);
	}

	if (words[0] == "cd")  // change directory.
	{
		out = SetCurrentDirectory(words[1].c_str());

		if (out == FALSE)
		{
			cout << "Could not change directory." << endl << "Error code: " << GetLastError() << endl;
		}

		return;
	}

	if (words[0] == "create")  // create a file.
	{
		handle = CreateFile(LPCSTR(words[1].c_str()), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		if (handle == INVALID_HANDLE_VALUE)
		{
			cout << "Could not create the file." << endl << "Error code: " << GetLastError() << endl;		 
		}

		CloseHandle(handle);
		return;
	}

	if (words[0] == "ls")  // list the directory (doesn't work if there is hebrew in the directory name).
	{
		handle = FindFirstFile(LPCSTR((string(cwd) + "/*").c_str()), &FindFileData);

		if (handle == INVALID_HANDLE_VALUE)
		{
			cout << "Could not list the files in the directory." << endl << "Error code: " << GetLastError() << endl;
			FindClose(handle);
			return;
		}

		do
		{
			cout << FindFileData.cFileName << endl;
		} while (FindNextFile(handle, &FindFileData) != FALSE);  // finds first file, and then loops the rest of the files. 

		if (GetLastError() != ERROR_NO_MORE_FILES)
		{
			cout << "Could not list the files in the directory." << endl << "Error code: " << GetLastError() << endl;
		}

		FindClose(handle);
		return;
	}

	if (words[0] == "secret")  // loads a dll and runs a function from it.
	{
		module = LoadLibrary(LPCSTR("secret.dll"));

		if (module == NULL)
		{
			cout << "Could not load the module." << endl << "Error code: " << GetLastError() << endl;
			FreeModule(module);
			return;
		}

		f = (func*)GetProcAddress(module, "TheAnswerToLifeTheUniverseAndEverything");  // func defined. gets the function address and converts to func* type.

		if (f == NULL)
		{
			cout << "Could not find the answer to the universe." << endl << "Error code: " << GetLastError() << endl;
			FreeModule(module);
			return;
		}

		cout << f() << endl;  // runs the function.

		FreeModule(module);
		return;
	}

	if (words[0] == "run")  // runs a program.
	{
		ZeroMemory(&si, sizeof(STARTUPINFO));
		ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

		for (i = 1; i < words.size(); i++)  // inserts the program name and the parameters into one string.
		{
			cmdLine = string(cmdLine) + words[i];
		}

		out = CreateProcess(NULL, LPSTR(cmdLine.c_str()), NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi);

		if (!out)
		{
			cout << "Could not execute the program." << endl << "Error code: " << GetLastError() << endl;
			return;
		}

		// this function gets a handle of a process and waits for it until it's over.
		WaitForSingleObject(pi.hProcess, INFINITE);

		// this function gets a handle of a process and a pointer to the exit code and puts the process exit code in the given address.
		out = GetExitCodeProcess(pi.hProcess, exitCode);

		if (!out)
		{
			cout << "Could not get the return value." << endl << "Error code: " << GetLastError() << endl;
			return;
		}

		cout << "Exit code: " << *exitCode << endl;

		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);

		return;
	}

	cout << "Wrong syntax." << endl;
}
