#include "Helper.h"
#include <Windows.h>
#include <iostream>

#define BUFFER_SIZE 1024

using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::endl;


typedef int func(void);


void executeCommand(vector<string> words);